package cups_admin

import "testing"

var testCases = []struct {
	input    string
	expected bool
}{
	{
		"serial:/dev/ttyS0?baud=115200",
		true,
	},
	{
		"usb://Samsung/ML-1640%20Series?serial=146RBKBZ100293M.",
		true,
	},
	{
		"socket://hostname",
		true,
	},
	{
		"socket://ip-address-or-hostname:631/?option=value&option=value",
		true,
	},
	{
		"http://ip-address-or-hostname:550/resource?option=value",
		true,
	},
	{
		"ipp://ip-address-or-hostname/resource",
		true,
	},
	{
		"ipp://ip-address-or-hostname:1010/resource?option=value&option=value",
		true,
	},
	{
		"ipps://ip-address-or-hostname/resource",
		true,
	},
	{
		"ipps://ip-address-or-hostname:1089/resource?option=value&option=value",
		true,
	},
	{
		"lpd://ip-address-or-hostname/queue",
		true,
	},
	{
		"lpd://username@ip-address-or-hostname/queue?option=value&option=value",
		true,
	},
	{
		"lpd1://username@ip-address-or-hostname/queue?option=value&option=value",
		false,
	},
	{
		"https://ip-address-or-hostname:3350/resource?option=value",
		true,
	},
	{
		"socket://192.168.0.1",
		true,
	},
	{
		"socket://1921.168.0.1",
		false,
	},
	{
		"socket://192.168.0.10:564/?option=value&option=value",
		true,
	},
	{
		"http://176.178.12.11:337/resource?option=value",
		true,
	},
	{
		"ipp://192.233.233.2/resource",
		true,
	},
	{
		"ipp://192.168.0.11:port-number/resource?option=value&option=value",
		false,
	},
	{
		"ipps://192.167.23.11/resource",
		true,
	},
	{
		"ipps://176.178.122.1:4433/resource?option=value&option=value",
		true,
	},
	{
		"lpd://196.122.33.12/queue",
		true,
	},
	{
		"lpd://username@192.168.1.254/queue?option=value&option=value",
		true,
	},
	{
		"lpd://username@1929.168.1.254/queue?option=value&option=value",
		false,
	},
	{
		"lpd1://username@ip-address-or-hostname/queue?option=value&option=value",
		false,
	},
	{
		"https://ip-address-or-hostname:4040/resource?option=value",
		true,
	},
	{
		"https://ip-address-or-hostname:port/resource?option=value",
		false,
	},
	{
		"",
		false,
	},
	{
		"http",
		false,
	},
	{
		"ipp",
		false,
	},
	{
		"https:/ip-address-or-hostname:3920/resource?option=value",
		false,
	},
	{
		"parallel:/dev/lp0",
		true,
	},
}

func TestUriValiadtor(t *testing.T) {
	for _, tc := range testCases {
		if ret := validatePrinterURI(tc.input); ret != tc.expected {
			t.Fatalf("%s--->%v,expected=%v", tc.input, ret, tc.expected)
		}
	}
}
