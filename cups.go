package cups_admin

import (
	"io/ioutil"
	"os/exec"
	"regexp"
	"sort"
	"strings"
)

const (
	lpinfo = "/usr/sbin/lpinfo"
	lsusb  = "/usr/bin/lsusb"
)

//function ListConnectedPrinters used to list connected printers
//this function used to test on embedded device
func listConnectedPrinters() []string {
	cmd := exec.Command(lpinfo, "-v")
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		panic(err)
	}
	if err := cmd.Start(); err != nil {
		panic(err)
	}
	output, err := ioutil.ReadAll(stdout)
	if err != nil {
		panic(err)
	}
	if err := cmd.Wait(); err != nil {
		panic(err)
	}
	uriSlice := []string{}
	separatedLines := regexp.MustCompile("[\n\r]+").Split(string(output), -1)
	for _, line := range separatedLines {
		uri := regexp.MustCompile("[\t ]+").Split(line, 2)
		if len(uri) >= 2 {
			if validatePrinterURI(uri[1]) {
				uriSlice = append(uriSlice, uri[1])
			}
		}
	}
	return uriSlice
}

//List connected usb devices
func listUsbDevices() []string {
	cmd := exec.Command(lsusb)
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		panic(err)
	}
	if err := cmd.Start(); err != nil {
		panic(err)
	}
	output, err := ioutil.ReadAll(stdout)
	if err != nil {
		panic(err)
	}
	if err := cmd.Wait(); err != nil {
		panic(err)
	}
	devSlice := []string{}
	separatedLines := regexp.MustCompile("[\n\r]+").Split(string(output), -1)
	for _, line := range separatedLines {
		usb := regexp.MustCompile("[\t ]+").Split(line, 7)
		if len(usb) >= 7 {
			devSlice = append(devSlice, usb[6])
		}
	}
	return devSlice
}

//FindValidPrinter first checks listConnectedPrinters then listUsbDevices and match their output
func FindValidPrinter() string {
	uris := listConnectedPrinters()
	ws := make([]int, len(uris))
	for i, uri := range uris {
		for _, usb := range listUsbDevices() {
			uriStems := regexp.MustCompile("[[:punct:]]+|(%20)+").Split(uri, -1)
			for _, uriStem := range uriStems {
				if strings.Contains(strings.ToLower(usb), strings.ToLower(uriStem)) {
					ws[i]++
				}
			}
		}
	}
	origWs := ws
	var index int
	sort.Ints(ws)
	for i, w := range origWs {
		print("w = ")
		println(w)
		if w == ws[len(ws)-1] {

			index = i
		}
	}
	return uris[index]
}

//FindUriMakeDeviceID function finds printer URI, make-and-model and device-id automatically
func FindUriMakeDeviceID() (uri, make, deviceId string) {
	cmd := exec.Command(lpinfo, "-l", "-v")
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		panic(err)
	}
	if err := cmd.Start(); err != nil {
		panic(err)
	}
	output, err := ioutil.ReadAll(stdout)
	if err != nil {
		panic(err)
	}
	if err := cmd.Wait(); err != nil {
		panic(err)
	}
	re := regexp.MustCompile("Device:[[:blank:]]*uri[[:blank:]]*=[[:blank:]]*(?P<uri>[^[:space:]]+).*make-and-model[[:blank:]]*=[[:blank:]]*(?P<make>[^\n\r\f]+).*device-id[[:blank:]]*=[[:blank:]]*(?P<device>[^\n\r\f]*).*")
	match := re.FindStringSubmatch(string(output))
	result := map[string]string{}
	if match != nil {
		for i, name := range re.SubexpNames() {
			if i != 0 {
				result[name] = match[i]
			}
		}
	}
	println(result["uri"])
	return
}

func validatePrinterURI(uri string) bool {
	if matched, err := regexp.MatchString(`(^(beh|ipp|ipps|ipp14|http|lpd|https|bjnp|socket|usb)://([[:alpha:]][[:word:]]+[@]{1}){0,1}(([_a-zA-Z]([_a-zA-Z0-9]|[-])+)|(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])[.]){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])))((:[0-9]+)[^:@]*|[^:@]*)$|^((serial|parallel):/dev/.*)$`, uri); err != nil {
		panic(err)
	} else {
		return matched
	}
	return false
}
